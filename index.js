require('dotenv').config()
const express = require('express')
const morgan = require('morgan')

const app = express()

const logger = require('./config/logger')
const indexApiRoute = require('./config/routes/api/index.api.routes')

// configure logging
if (process.env.NODE_ENV !== 'test') {
  const logFile = process.env.NODE_ENV === 'development'
    ? 'dev'
    : 'combined'
  app.use(morgan(logFile, { stream: logger.stream }))
}

// route
app.use('/api', indexApiRoute)

// error handler
app.use((err, req, res, next) => {

  // winston logging
  logger.error(`${err.httpStatusCode || 500} - ${err.response.data.error.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  res.status(err.httpStatusCode)
     .json({ code: err.httpStatusCode, error: err.response.data.error })
})

module.exports = app