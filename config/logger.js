const appRoot = require('app-root-path')
const { createLogger, format, transports } = require('winston')
const fs = require('fs')

if (!fs.existsSync(`${appRoot}/logs`)){
  fs.mkdirSync(`${appRoot}/logs`);
}

const options = {
  file: {
    combined: {
      level: 'info',
      filename: `${appRoot}/logs/app.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
    },
    error: {
      level: 'error',
      filename: `${appRoot}/logs/error.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
    }
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
    format: format.simple()
  },
}

const logger = createLogger({
  transports: [
    new transports.File(options.file.combined),
    new transports.File(options.file.error),
    new transports.Console(options.console)
  ],
  exitOnError: false, // do not exit on handled exceptions
})

logger.stream = {
  write: function(message, encoding) {
    logger.info(message)
  },
}

module.exports = logger
