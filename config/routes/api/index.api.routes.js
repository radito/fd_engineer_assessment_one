const router = require('express').Router()
const indexCtr = require('./../../../app/controllers/index.controller')

router
  .get('/feed', indexCtr.index)

module.exports = router