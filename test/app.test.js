process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')

const should = chai.should()
const expect = chai.expect

const server = require('./../index')

chai.use(chaiHttp)

describe('Test response to client', function() {
  describe('index end point (\'/api\')', function() {
    it('Should has status code of 200', function(done) {
      chai.request(server)
        .get('/api/feed')
        .end(function(err, result) {
          result.should.have.status(200)
          result.res.statusMessage.should.equal('OK')
          done()
        })
    })
    it('Response value should be an object', function(done) {
      chai.request(server)
        .get('/api/feed')
        .end(function(err, result) {
          expect(JSON.parse(result.text)).should.be.an('object')
          done()
        })
    })
    it('Response value should not null', function(done) {
      chai.request(server)
        .get('/api/feed')
        .end(function(err, result) {
          result.text.should.not.equal('null')
          done()
        })
    })
    it('Response value should not empty', function(done) {
      chai.request(server)
        .get('/api/feed')
        .end(function(err, result) {
          result.text.should.not.be.empty
          done()
        })
    })
    it('[page 1] Response should contains five (or less) posts', function(done) {
      chai.request(server)
        .get('/api/feed')
        .end(function(err, result) {
          const data = JSON.parse(result.text).content.data

          data.should.have.length.below(6)
          done()
        })
    })
    it('[page 2] Response should contains five (or less) posts', function(done) {
      chai.request(server)
        .get('/api/feed?page=2')
        .end(function(err, result) {
          const data = JSON.parse(result.text).content.data

          data.should.have.length.below(6)
          done()
        })
    })
  })
})