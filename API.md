**Show User Feed**
* **URL**

  `/api/feed`
* **Method:**
  
  `GET`
*  **URL Params**

   **Required:**
 
   `page=[integer]`

* **Success Response:**

  * **code:** 200 <br />
    **content:** `{data: [object], paging: [object]}`
 
* **Error Response:**

  * **code:** 400 <br />
    **error:** `{message : "error message", type: "error type", code: error code, fbtrace_id: "trace id"}`

* **Sample Call:**

  `/api/feed?page=3`