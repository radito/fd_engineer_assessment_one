const axios = require('axios')

const fbAccessToken = process.env.FB_ACCESS_TOKEN
const fbUserId = process.env.FB_USER_ID
const urlFields = "id,name,message,attachments"
const urlBase = `https://graph.facebook.com/v3.0/${fbUserId}/feed?access_token=${fbAccessToken}&fields=${urlFields}`

module.exports = {
  index(req, res, next) {
    const page = req.query.page || 1
    const limit = 5
    const offset = (page - 1) * limit

    axios
      .get(`${urlBase}&offset=${offset}&limit=${limit}`)
      .then(response => {
        res.json({ code: 200, content: response.data })
      })
      .catch(error => {
        if (error.response) {
          error.httpStatusCode = 400

          return next(error)
        }

        res.json(error.config)
      })
  }
}