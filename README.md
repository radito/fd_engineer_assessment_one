# FD Engineer Assessment #1

Female Daily engineer assessment test (number 1). The project is about creating endpoint for streaming facebook feed of user.

## Getting Started

### Prerequisites


```
node 6.0 or above
npm 5.0 or above
```

### Setting Up `.env` File
```
# Rename `.env.example` to '.env' and fill the variables with value
```

### Generate Facebook Access Token
```
1. Log in to Facebook or Facebook Developer
2. Go to https://developers.facebook.com/tools/explorer
3. Select 'Get User Access Token' on 'Get Token' button
4. Select permissions (at least: user_posts & user_status)
5. Copy generated token into .env file ('FB_ACCESS_TOKEN' variable)
6. Fill 'FB_USER_ID' with unique user id or simply fill with 'me'
```

### Install Packages
```
$ npm install
```

### Running App
```
$ npm start

# for development
$ npm run dev

access `localhost:3000/api/feed'
```

## Running the tests
```
npm run test
```

## Endpoints documentation
```
API.md
```

## Built With

* [Express](https://expressjs.com/) - The web framework used

## Authors

* **Raditya Arya** - [Linkedin](https://linkedin.com/in/radityaarya), [GitHub](https://github.com/radityaarya)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details